# Introduction

*DAQling* is an open-source lightweight C++ software framework, to be used as core for data acquisition systems of small and medium-sized experiments.

The framework offers a complete DAQ ecosystem, including communication layer with a selection of back-ends (including the widespread *ZeroMQ* messaging library), configuration management based on the *JSON* format, control of distributed applications based on *Python*, extendable operational monitoring with web-based visualization, and a set of generic utilities.

The framework provides automated host and build environment setup based on the *Ansible* automation tool. Finally, the end-user code is wrapped in so-called “Modules”, that can be loaded at configuration time, and implement specific roles according to the data flow of the experiment.

## Project overview

*DAQling* can be cloned from [https://gitlab.cern.ch/ep-dt-di/daq/daqling](https://gitlab.cern.ch/ep-dt-di/daq/daqling).

*DAQling_top* is the top-level skeleton project, which includes *DAQling* as a `git submodule`. It should be forked to start a custom project. Setting up the "repository mirroring", will allow the forked project to keep the `dist-master` branch up-to-date with *DAQling_top*.  
It can be found at [https://gitlab.cern.ch/ep-dt-di/daq/daqling_top](https://gitlab.cern.ch/ep-dt-di/daq/daqling_top).


## Getting started

The framework is supported on *Alma Linux 9*. Host configuration and build instructions can be found in the *DAQling* `README.md` file.

Instructions on how to run a demo or custom system are also present in the *DAQling* `README.md` file.

## DAQling-based experiments

A few small-medium experiments are using or evaluating DAQling for their DAQ system.

* [FASER](https://faser.web.cern.ch/). Source code at [faser-daq](https://gitlab.cern.ch/faser/online/faser-daq).
* [NA61/SHINE](https://shine.web.cern.ch/).
* LACTEL (evaluating).
* [SHiP](https://ship.web.cern.ch/) (evaluating).